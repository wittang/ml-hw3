import sys
import processor
import distance as dst
import operator
import numpy as np

def get_Centroids(train_Data):
	train_Matrix = np.sum(train_Data, axis = 0)
	return (1.0/train_Data.shape[0]) * train_Matrix


def predict(pos_distance, neg_distance):
	if pos_distance < neg_distance:
		return "pos"
	else:
		return "neg"

def main():
	accuracies = []
	precisions = []
	recalls = []
	print("Processing...")
	vectors, k, metric = processor.process(sys.argv)
	vector_Subsets = processor.split_List(vectors, 5)
	for index in range(5):
		print("\n--- Test " + str(index + 1) + " ---")
		performance = { 'true_Pos' : 0, 'false_Pos' : 0, 'true_Neg' : 0, 'false_Neg' : 0 }
		train_Vectors = [elem for x in [x for ind, x in enumerate(vector_Subsets) if ind != index] for elem in x]
		test_Vectors = vector_Subsets[index]
		word_Indices = processor.get_WordIndices(train_Vectors)
		pos_Matrix, neg_Matrix = processor.get_ClassMatrices(train_Vectors, word_Indices)
		pos_Centroids, neg_Centroids = get_Centroids(pos_Matrix), get_Centroids(neg_Matrix)
		print("Predicting...\n")
		for (test_VectorDict, test_Words, test_Label) in test_Vectors:
	 		test_Vector = processor.create_Vector(test_Words, word_Indices, test_VectorDict)
	 		pos_distance, neg_distance = dst.find_Distance(metric, pos_Centroids, test_Vector), dst.find_Distance(metric, neg_Centroids, test_Vector)
	 		prediction = predict(pos_distance, neg_distance)
			processor.update_PerfCounts(performance, test_Label, prediction)
		accuracy, precision, recall = processor.calc_Performance(performance['true_Pos'], performance['false_Pos'], performance['true_Neg'], performance['false_Neg'])
		print(processor.perf_toString(accuracy, precision, recall))
		accuracies.append(accuracy)
		precisions.append(precision)
		recalls.append(recall)
	print("\n--- Nearest-Centroid Classification ---")
	print(processor.feature_toString(sys.argv))
	print(processor.allPerf_toString(accuracies, precisions, recalls))



main()
