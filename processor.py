import os
import string
import random
import numpy as np

pos_Dir = "review_polarity/txt_sentoken/pos"
neg_Dir = "review_polarity/txt_sentoken/neg"

def get_ProcessOpt(args):
	input_Type = "frequency"
	punct_Flag = True
	k = 1
	metric = "euclidean"
	if not args:
		return input_Type, punct_Flag, k, metric
	else:
		for index in range(len(args)):
			arg = args[index].lower()
			if arg in ["--frequency", "--binary" ]:
				input_Type = arg[2:]
			elif arg in ["--nopunctuation", "--punctuation"]:
				if arg == "--nopunctuation":
					punct_Flag = False
			elif arg in ["--metric=euclidean", "--metric=manhattan"]:
				metric = arg[9:]
			else:
				try: 
					k = int(arg[4])
					if k%2 == 0:
						k = k - 1
				except:
					k = 1
	return input_Type, punct_Flag, k, metric

def create_Vector(words, word_Indices, feat_Vector):
	vector = [0] * len(word_Indices)
	for word in words:
		if word in word_Indices:
			index = word_Indices[word]
			feat = feat_Vector[word]
			vector[index] = feat
	#print(sum(vector))
	return vector

def add_WordIndices(words, word_IndexDict):
	for word in words:
		if word not in word_IndexDict:
			word_IndexDict[word] = len(word_IndexDict)

def get_WordIndices(train_Vectors):
	word_Indices = {}
	for (vector_Dict, words, label) in train_Vectors:
		add_WordIndices(words, word_Indices)
	return word_Indices

def get_ClassMatrices(train_Data, word_Indices):
	pos_Array, neg_Array = [], []
	for (train_VectorDict, train_Words, train_Label) in train_Data:
		if train_Label == "pos":
			pos_Array.append(create_Vector(train_Words, word_Indices, train_VectorDict))
		else:
			neg_Array.append(create_Vector(train_Words, word_Indices, train_VectorDict))
	return np.array(pos_Array), np.array(neg_Array)

def split_List(list, multiple):
	new_List = []
	length = len(list)/multiple
	for index in range(multiple):
		if index < multiple - 1:
			new_List.append(list[(index*length):(index+1)*length])
		else:
			new_List.append(list[(index*length):])
	return new_List

def get_TrainingFiles(pos_Dir, neg_Dir):
	pos_Files, neg_Files = [], []
	pos_DirFiles, neg_DirFiles = os.listdir(pos_Dir), os.listdir(neg_Dir)
	for index in range(len(pos_DirFiles)):
		pos_Files.append(pos_Dir + "/" + pos_DirFiles[index])
		neg_Files.append(neg_Dir + "/" + neg_DirFiles[index])
	random.shuffle(pos_Files)
	random.shuffle(neg_Files)
	return pos_Files, neg_Files

def get_Freq(words):
	freq_Dict = {}
	for word in words:
		if word in freq_Dict:
			freq_Dict[word] += 1
		else:
			freq_Dict[word] = 1
	return freq_Dict



def try_removePunct(sentence, punct_Flag):
	if punct_Flag:
		sentence = sentence.translate(None, string.punctuation)
	return sentence

def add_VectorData(vector, data, input_Type):
	if input_Type == "frequency":
		freq_Dict = get_Freq(data)
		for word in data:
			vector[word] = freq_Dict[word]
	else:
		for word in data:
			vector[word] = 1

def create_VectorDict(training_File, input_Type, punct_Flag):
	vector_Dict = {}
	training_Data = try_removePunct(open(training_File).read(), punct_Flag)
	training_Words = training_Data.split()
	add_VectorData(vector_Dict, training_Words, input_Type)
	return vector_Dict, training_Words

# def add_Words(words, word_List):
# 	for word in words:
# 		word_List.add(word)

def process(args):
	input_Type, punct_Flag, k, metric = get_ProcessOpt(args)
	vectors = []
	pos_Files, neg_Files = get_TrainingFiles(pos_Dir, neg_Dir)
	for index in range(len(pos_Files)):
		vector_Dict, words = create_VectorDict(pos_Files[index], input_Type, punct_Flag)
		vectors.append((vector_Dict, words, "pos"))
		vector_Dict, words = create_VectorDict(neg_Files[index], input_Type, punct_Flag)
		vectors.append((vector_Dict, words, "neg"))
	return vectors, k, metric

def calc_Acc(true_Pos, false_Pos, true_Neg, false_Neg):
	return (float(true_Pos + true_Neg)/(true_Pos + true_Neg + false_Pos + false_Neg))

def calc_Perc(true_Pos, false_Pos, true_Neg, false_Neg):
	try:
		pos_Perc = float(true_Pos)/(true_Pos + false_Pos)
		neg_Perc = float(true_Neg)/(true_Neg + false_Neg)
		return (float(pos_Perc + neg_Perc)/2)
	except:
		return 0

def calc_Recall(true_Pos, false_Pos, true_Neg, false_Neg):
	try:
		pos_Recall = float(true_Pos)/(true_Pos + false_Neg)
		neg_Recall = float(true_Neg)/(true_Neg + false_Pos)
		return (float(pos_Recall + neg_Recall)/2)	
	except:
		return 0

def update_PerfCounts(perf_Dict, test_Label, prediction):
	if test_Label == 'pos':
		if test_Label == prediction:
			perf_Dict["true_Pos"] += 1
		else:
			perf_Dict["false_Neg"] += 1
	else:
		if test_Label == prediction:
			perf_Dict["true_Neg"] += 1
		else:
			perf_Dict["false_Pos"] += 1



def calc_Performance(true_Pos, false_Pos, true_Neg, false_Neg):
	accuracy = calc_Acc(true_Pos, false_Pos, true_Neg, false_Neg)
	precision = calc_Perc(true_Pos, false_Pos, true_Neg, false_Neg)
	recall = calc_Recall(true_Pos, false_Pos, true_Neg, false_Neg)
	return accuracy, precision, recall

def perf_toString(accuracy, precision, recall):
	return "Accuracy: " + str(accuracy) + "\nPrecision: " + str(precision) + "\nRecall: " + str(recall)

def find_MaxMin(performances):
	max_Index = performances.index(max(performances)) + 1
	max_Value = round(max(performances),3)
	min_Index = performances.index(min(performances)) + 1
	min_Value = round(min(performances),3)
	return max_Index, max_Value, min_Index, min_Value

def avg_toString(performances, perf_Type):
	avg_Perfomance = (sum(performances))/len(performances)
	return "Average " + perf_Type + ": " + str(avg_Perfomance) + "\n"

def maxMin_toString(performances, perf_Type):
	max_Index, max_Value, min_Index, min_Value = find_MaxMin(performances)
	return "Max " + perf_Type + " at fold " + str(max_Index) + ": " + str(max_Value) + "\tMin " + perf_Type + " at fold " + str(min_Index) + ": " + str(min_Value) + "\n"

def allPerf_toString(accuracies, precisions, recalls):
	header = "--- Overall Performance ---\n"
	acc_String = avg_toString(accuracies, "Accuracy")
	prec_String = avg_toString(precisions, "Precision")
	rec_String = avg_toString(recalls, "Recall")
	mmAcc_String = maxMin_toString(accuracies, "Accuracy")
	mmPrec_String = maxMin_toString(precisions, "Precision")
	mmRec_String = maxMin_toString(recalls, "Recall")
	return header + acc_String + prec_String + rec_String + mmAcc_String + mmPrec_String + mmRec_String

def feature_toString(args):
	input_type, punct, k, metric = get_ProcessOpt(args)
	return "FEATURE TYPE: " + input_type.upper() + "\nPUNCTUATION: " + str(punct).upper() + "\nMETRIC: " + metric.upper()
	



