import sys
import processor 
import distance as dst
import operator
import numpy as np

def predict(distances, pos_distances, neg_distances):
	class_Count = {'pos' : 0, 'neg' : 0}
	for distance in distances:
		if distance in pos_distances:
			if distance in neg_distances:
				class_Count['neg'] += 1
			class_Count['pos'] += 1
		else:
			class_Count['neg'] += 1
	return max(class_Count.iteritems(), key=operator.itemgetter(1))[0]

def main():
	accuracies = []
	precisions = []
	recalls = []
	print("Processing...")
	vectors, k, metric = processor.process(sys.argv)
	vector_Subsets = processor.split_List(vectors, 5)
	for index in range(5):
		print("\n--- Test " + str(index + 1) + " ---")
		performance = { 'true_Pos' : 0, 'false_Pos' : 0, 'true_Neg' : 0, 'false_Neg' : 0 }
		train_Vectors = [elem for x in [x for ind, x in enumerate(vector_Subsets) if ind != index] for elem in x]
		test_Vectors = vector_Subsets[index]
		word_Indices = processor.get_WordIndices(train_Vectors)
		pos_Matrix, neg_Matrix = processor.get_ClassMatrices(train_Vectors, word_Indices)
		print("Predicting...\n")
		test_Index = 0
		for (test_VectorDict, test_Words, test_Label) in test_Vectors:
			test_Index += 1
	 		test_Vector = processor.create_Vector(test_Words, word_Indices, test_VectorDict)
	 		pos_distances, neg_distances = dst.find_Distance(metric, pos_Matrix, test_Vector), dst.find_Distance(metric, neg_Matrix, test_Vector)
	 		distances = np.sort(np.append(pos_distances, neg_distances))[:k]
	 		prediction = predict(distances, pos_distances, neg_distances)
	 		result = test_Label == prediction
	 		print("Document " + str(test_Index) + "\tLabel: " + test_Label + "\tPrediction: " + prediction + "\t\tCorrect: " + str(result))
	 		processor.update_PerfCounts(performance, test_Label, prediction)
		accuracy, precision, recall = processor.calc_Performance(performance['true_Pos'], performance['false_Pos'], performance['true_Neg'], performance['false_Neg'])
		print(processor.perf_toString(accuracy, precision, recall))
		accuracies.append(accuracy)
		precisions.append(precision)
		recalls.append(recall)
	print("\n--- K-Nearest-Neighbor Classification, K = " + str(k) + " ---")
	print(processor.feature_toString(sys.argv))
	print(processor.allPerf_toString(accuracies, precisions, recalls))



main()


