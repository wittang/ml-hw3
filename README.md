Wilson Tang
107513801
CSE353

Necessary Files:
review_polarity directory with movie review files
distance.py
processor.py
knn.py
ncentroid.txt

To begin: Open terminal and navigate to location of files

train-tagger.py
To run the KNN classifier, in the terminal, enter: python knn.py [--frequency | --binary] [--nopunctuation | punctuation] --k=[#] --metric==[euclidean | manhattan]
WITHOUT the brackets. 
The values within brackets are the options to choose between as such: [ Option1 | Option2]
Frequency or Binary represent the type of feature you want the input vectors to be. 
Nopunctuation or punctuation determines whether or not to keep punctuation when processing reviews.
The [#] is the number of neighbors to look for and predict with.
Euclidean or manhattan is the metric or distance formula to use. 


freq-tagger.py
To run the Nearest Centroid classifier, in the terminal, enter: python ncentroid.py [--frequency | --binary] [--nopunctuation | punctuation] --metric==[euclidean | manhattan]
WITHOUT the brackets
The values within brackets are the options to choose between as such: [ Option1 | Option2]
Frequency or Binary represent the type of feature you want the input vectors to be. 
Nopunctuation or punctuation determines whether or not to keep punctuation when processing reviews.
Euclidean or manhattan is the metric or distance formula to use. 


