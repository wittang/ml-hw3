import numpy as np

def euclidean(train_Data, test_Data):
	sum_squaredDiff = 0
	squared_Diff = (train_Data - test_Data)**2
	try:
		sum_squaredDiff = np.sum(squared_Diff, axis=1)
	except:
		sum_squaredDiff = np.sum(squared_Diff, axis=0)
	return np.sqrt(sum_squaredDiff)

def manhattan(train_Data, test_Data):
	absolute_Diff = abs(train_Data - test_Data)
	try: 
		return np.sum(absolute_Diff, axis = 1)
	except:
		return np.sum(absolute_Diff, axis = 0)

def find_Distance(metric, train_Data, test_Data):
	if metric == 'euclidean':
		return euclidean(train_Data, test_Data)
	else:
		return manhattan(train_Data, test_Data)

